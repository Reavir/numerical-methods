#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

double func(double x){
    double y;
    y = cos(x) - x;
    return y;
}

int main()
{
    double tolerance = pow(10, -8);
    double l_bound = 0;
    double r_bound = 1;
    int iteration = 0;
    if(func(l_bound) * func(r_bound) < 0){
        cout << "root is in the initial interval" << endl;
    }

    double middle;
    middle = (l_bound + r_bound)/2;

    while((r_bound - l_bound)/middle >= tolerance){
        if(func(l_bound) * func(middle) < 0){
            iteration += 1;
            r_bound = middle;
            middle = (l_bound + r_bound)/2;
        }
        else if((func(r_bound) * func(middle) < 0)){
            iteration += 1;
            l_bound = middle;
            middle = (l_bound + r_bound)/2;
        }
    }
    cout <<"The root value is "<< setprecision(8) << middle << endl;
    cout <<"Iterations: "<< iteration << endl;


    return 0;
}
